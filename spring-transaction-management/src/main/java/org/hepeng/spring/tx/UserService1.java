package org.hepeng.spring.tx;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author he peng
 */
public interface UserService1 {

    UserEntity getUserById(Long uid);

    @Transactional(rollbackFor = Throwable.class)
    void saveUser1(UserEntity user);
}
