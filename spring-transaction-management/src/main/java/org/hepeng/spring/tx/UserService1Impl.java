package org.hepeng.spring.tx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author he peng
 */

@Service
public class UserService1Impl implements UserService1 {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public UserEntity getUserById(Long uid) {
        return null;
    }

    @Override
    public void saveUser1(UserEntity user) {
        Object[] args = {user.getNickname() , user.getRealname() , user.getPhone() ,
                user.getLoginPassword() , user.getPayPassword() , user.getCreateTime() ,
                user.getUpdateTime() , user.getDeleted()};
        this.jdbcTemplate.update("insert into `user` " +
                "(nickname ,realname , phone, login_password , pay_password , " +
                "create_time , update_time , deleted) " +
                "values (? ,? , ?, ? , ? , ? , ? , ?)" , args);
    }
}
