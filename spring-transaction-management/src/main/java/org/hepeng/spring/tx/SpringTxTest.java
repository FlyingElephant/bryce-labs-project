package org.hepeng.spring.tx;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @author he peng
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class SpringTxTest {

    @Autowired
    UserService1 userService1;

    UserEntity userEntity;

    @Before
    public void newUserEntity() {
        userEntity = new UserEntity();
        userEntity.setCreateTime(new Date());
        userEntity.setUpdateTime(new Date());
        userEntity.setDeleted((byte)1);
        userEntity.setLoginPassword("xxsdas3rr");
        userEntity.setPayPassword("dasfdsf33qr2f");
        userEntity.setNickname("SSS");
        userEntity.setRealname("S@GGSS");
        userEntity.setPhone(12345678955L);
    }

    @Test
    public void saveUser1() throws Exception {
        userService1.saveUser1(userEntity);
    }
}
