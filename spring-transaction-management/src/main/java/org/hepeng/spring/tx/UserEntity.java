package org.hepeng.spring.tx;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class UserEntity implements Serializable {

    private Long id;

    private String nickname;

    private String realname;

    private Long phone;

    private String loginPassword;

    private String payPassword;

    private Date createTime;

    private Date updateTime;

    private Byte deleted;

}
