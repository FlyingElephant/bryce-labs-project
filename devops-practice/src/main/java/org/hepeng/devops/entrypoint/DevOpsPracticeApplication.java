package org.hepeng.devops.entrypoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author he peng
 */

@SpringBootApplication(scanBasePackages = {"org.hepeng.devops"})
public class DevOpsPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevOpsPracticeApplication.class , args);
    }
}
