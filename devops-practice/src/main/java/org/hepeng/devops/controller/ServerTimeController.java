package org.hepeng.devops.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author he peng
 */

@RestController
public class ServerTimeController {

    @GetMapping("/server-time")
    public ResponseEntity<Map<String , Object>> getServerTime() {
        Map<String , Object> payloadMap = new HashMap<>();
        payloadMap.put("server time" , LocalDateTime.now(ZoneId.systemDefault()));
        return ResponseEntity.ok(payloadMap);
    }
}
